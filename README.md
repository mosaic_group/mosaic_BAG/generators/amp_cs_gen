# amp_cs_gen

BAG2 new style generator of the common source amplifier 
described at https://github.com/ucb-art/BAG_XBase_demo/blob/master/tutorial_files/3_analogbase.ipynb

## Getting started

Starting from virtuoso_template,
simply add this generator via 
```
cd virtuoso_template
./submodule.sh add <ssh-url-for-this-repo>
./configure
make -C amp_cs_gen finfet
```

