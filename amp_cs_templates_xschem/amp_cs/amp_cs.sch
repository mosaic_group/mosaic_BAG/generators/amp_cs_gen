v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 550 -250 550 -230 {
lab=VSS}
N 300 -40 300 -20 {
lab=VSS}
N 550 -170 550 -150 {
lab=VSS}
N 490 -200 510 -200 {
lab=VSS}
N 550 -200 580 -200 {
lab=VSS}
N 240 -70 260 -70 {
lab=vin}
N 300 -120 300 -100 {
lab=vout}
N 300 -70 330 -70 {
lab=VSS}
N 300 -170 300 -150 {
lab=vout}
N 240 -200 260 -200 {
lab=vbias}
N 300 -250 300 -230 {
lab=VDD}
N 300 -200 370 -200 {
lab=VDD}
N 330 -70 370 -70 {
lab=VSS}
N 580 -200 620 -200 {
lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 280 -70 0 0 {name=XN
w=500n
l=60n
nf=1
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 280 -200 0 0 {name=XP
w=500n
l=60n
nf=1
spiceprefix=X
}
C {devices/iopin.sym} 50 -260 0 0 {name=p1 lab=VDD}
C {devices/iopin.sym} 50 -240 0 0 {name=p2 lab=VSS}
C {devices/ipin.sym} 70 -210 0 0 {name=p3 lab=vin}
C {devices/opin.sym} 80 -210 0 0 {name=p4 lab=vout}
C {devices/lab_pin.sym} 550 -150 0 0 {name=l1 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 490 -200 0 0 {name=l2 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 550 -250 0 0 {name=l3 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 620 -200 2 0 {name=l4 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 300 -20 2 0 {name=l5 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 370 -70 2 0 {name=l6 sig_type=std_logic lab=VSS
}
C {devices/lab_pin.sym} 240 -70 0 0 {name=l7 sig_type=std_logic lab=vin
}
C {devices/lab_pin.sym} 300 -120 2 0 {name=l8 sig_type=std_logic lab=vout
}
C {devices/lab_pin.sym} 300 -150 0 0 {name=l9 sig_type=std_logic lab=vout
}
C {devices/lab_pin.sym} 240 -200 0 0 {name=l10 sig_type=std_logic lab=vbias
}
C {devices/lab_pin.sym} 300 -250 2 0 {name=l11 sig_type=std_logic lab=VDD
}
C {devices/lab_pin.sym} 370 -200 2 0 {name=l12 sig_type=std_logic lab=VDD
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 530 -200 0 0 {name=XDUM
w=500n
l=60n
nf=1
spiceprefix=X
}
C {devices/ipin.sym} 70 -190 0 0 {name=p5 lab=vbias}
