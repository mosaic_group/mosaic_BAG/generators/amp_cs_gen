import os
import pkg_resources
from typing import *

from bag.design import Module

from .params import amp_cs_layout_params

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/amp_cs_templates',
                         'netlist_info', 'amp_cs.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library amp_cs_templates cell amp_cs.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            lch='channel length in meters.',
            layout_params='amp_cs_layout_params layout parameter object.',
            dum_info='Dummy information data structure',
        )

    def design(self,
               lch: float,
               layout_params: amp_cs_layout_params,
               dum_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        w_p = layout_params.w_dict['load']
        w_n = layout_params.w_dict['amp']
        intent_p = layout_params.intent_dict['load']
        intent_n = layout_params.intent_dict['amp']

        fg_amp = layout_params.fg_dict['amp']
        fg_load = layout_params.fg_dict['load']

        # set transistor parameters
        self.instances['XP'].design(w=w_p, l=lch, intent=intent_p, nf=fg_load)
        self.instances['XN'].design(w=w_n, l=lch, intent=intent_n, nf=fg_amp)

        # handle dummy transistors
        self.design_dummy_transistors(dum_info, 'XDUM', 'VDD', 'VSS')
