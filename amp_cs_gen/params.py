#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.simulation.simulation_output import *
from sal.testbench_params import *

from dc_tb.testbench import testbench as dc_tb_testbench
from dc_tb.params import dc_tb_params
from ac_tran_tb.testbench import testbench as ac_tran_tb_testbench
from ac_tran_tb.params import ac_tran_tb_params


@dataclass
class amp_cs_layout_params(LayoutParamsBase):
    """
    Parameter class for amp_cs_gen

    Args:
    ----
    lch : float
        Channel length of the transistors

    w_dict : Dict[str, Union[float, int]]
        NMOS/PMOS width dictionary

    intent_dict : Dict[str, str]
        NMOS/PMOS threshold flavor dictionary

    fg_dict : Dict[str, int]
        number of fingers dictionary

    ndum : int
        Number of fingers in NMOS transistor

    ptap_w : Union[float, int]
        Width of P substrate contact

    ntap_w : Union[float, int]
        Width of the N substrate contact

    top_layer: int
        Top metal Layer used in Layout

    show_pins : bool
        True to create pin labels
    """

    lch: float
    w_dict: Dict[str, Union[float, int]]
    intent_dict: Dict[str, str]
    fg_dict: Dict[str, int]
    ndum: int
    ptap_w: Union[float, int]
    ntap_w: Union[float, int]
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> amp_cs_layout_params:
        return amp_cs_layout_params(
            lch=min_lch,
            w_dict={
                'amp': 10,
                'load': 10
            },
            intent_dict={
                'amp': 'standard',
                'load': 'standard'
            },
            fg_dict={
                'amp': 2,
                'load': 4
            },
            ndum=4,
            ptap_w=10,
            ntap_w=10,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> amp_cs_layout_params:
        # NOTE: sky130 can use min_lch as L for standard,
        #       but not for lvt flavor,
        #       that would cause ngspice simulation errors like:
        # -------------------------------------------------
        # Error on line:
        # m.xxdut.xxp.xm1.msky130_fd_pr__pfet_01v8_lvt vout vbias vdd vdd +
        #   xxdut.xxp.xm1:sky130_fd_pr__pfet_01v8_lvt__model l=    3.000000000000000e-01
        #   w=    3.000000000000000e+00     nf=    4.000000000000000e+00     ad=    4.349999999999999e-01
        #   as=    6.525000000000000e-01     pd=    4.160000000000000e+00     ps=    6.240000000000000e+00
        #   nrd=    9.666666666666666e-02     nrs=    9.666666666666666e-02     sa=    0.000000000000000e+00
        #   sb=    0.000000000000000e+00     sd=    0.000000000000000e+00     m=    1.000000000000000e+00
        # could not find a valid modelname
        # -------------------------------------------------

        l_factor = 3.0  # see comment above

        return amp_cs_layout_params(
            lch=min_lch * l_factor,  # see comment above
            w_dict={
                'amp': 10.0 / l_factor * min_lch,  # see comment above
                'load': 10.0 / l_factor * min_lch  # see comment above
            },
            intent_dict={
                'amp': 'standard',
                'load': 'lvt'
            },
            fg_dict={
                'amp': 2,
                'load': 4
            },
            ndum=4,
            ptap_w=10 * min_lch,
            ntap_w=10 * min_lch,
            show_pins=True,
        )


@dataclass
class amp_cs_measurement_dc_params(MeasurementParamsBase):
    plot: bool
    testbench_params_by_name: Dict[str, TestbenchParams]  # Key: testbench name

    @classmethod
    def defaults(cls, min_lch: float) -> amp_cs_measurement_dc_params:
        dut = DUT(lib="amp_cs_generated", cell="amp_cs")

        dc_defaults = dc_tb_params(
            dut=dut,  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    DUTTerminal(term_name='vbias', net_name='vbias'),
                ],
                v_sources=[
                    DCSignalSource(source_name='vbias',
                                   plus_net_name='vbias',
                                   minus_net_name='VSS',
                                   bias_value='vbias',
                                   cdf_parameters={}),
                ],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            gain_fb=400.0,
            vimax=1.0,
            vimin=1.0,
            vbias=0.185,
            vdd=1.0,
            voutref=0.5,
            vout_start=0.1,
            vout_stop=0.9,
            vout_num=100,
        )

        return amp_cs_measurement_dc_params(
            plot=True,
            testbench_params_by_name={
                'dc_tb': dc_defaults,
            }
        )


@dataclass
class amp_cs_measurement_ac_tran_params(MeasurementParamsBase):
    plot: bool
    testbench_params_by_name: Dict[str, TestbenchParams]  # Key: testbench name

    @classmethod
    def defaults(cls, min_lch: float) -> amp_cs_measurement_ac_tran_params:
        dut = DUT(lib="amp_cs_generated", cell="amp_cs")

        stimuli_pwl_file = os.path.join(os.path.dirname(__file__), "stimuli/ac_tran_tb_step.data")
        stimuli_pwl_data: str
        with open(stimuli_pwl_file, "r") as f:
            lines = f.readlines()
            t_v_list = ' '.join([line.strip() for line in lines])
            stimuli_pwl_data = f"pwl({t_v_list})"

        ac_tran_defaults = ac_tran_tb_params(
            dut=dut,
            dut_wrapper_params=None,
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    DUTTerminal(term_name='vbias', net_name='vbias'),
                ],
                v_sources=[
                    DCSignalSource(source_name='vbias',
                                   plus_net_name='vbias',
                                   minus_net_name='VSS',
                                   bias_value='vbias',
                                   cdf_parameters={}),
                ],
                i_sources=[],
                instance_cdf_parameters={
                    'VIN': {
                        #    'acm': 0,
                        'fileName': stimuli_pwl_file,   # virtuoso flow
                        'value': "ac {vinac} " + stimuli_pwl_data,  # ngspice
                    }
                }
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={
                    'vdd_tran': SimulationOutputVoltage(analysis='tran', signal='vdd', quantity=VoltageQuantity.V)
                }
            ),
            vbias=0.185,
            vdd=1.0,
            vinac=1.0,
            vindc=0.56,
            fstart=1e6,
            fstop=100e9,
            fndec=20,
            tsim=100e-9,   # 100ns, was 2e-9,
            tstep=1e-12,
            cload=200e-15,  # 200fF, was 40e-15
            use_cload=True
        )

        return amp_cs_measurement_ac_tran_params(
            plot=True,
            testbench_params_by_name={
                'ac_tran_tb': ac_tran_defaults
            }
        )


@dataclass
class amp_cs_params(GeneratorParamsBase):
    layout_parameters: amp_cs_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> amp_cs_params:
        return amp_cs_params(
            layout_parameters=amp_cs_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[
                amp_cs_measurement_dc_params.defaults(min_lch=min_lch),
                amp_cs_measurement_ac_tran_params.defaults(min_lch=min_lch)
            ]
        )
