import matplotlib.pyplot as plt
import numpy as np
import os
from pprint import pprint
import scipy.interpolate
import scipy.optimize
from typing import *

import bag.data

from sal.log import info, error
from sal.testbench_base import TestbenchBase
from sal.simulation.measurement_base import *
from sal.simulation.simulation_mode import SimulationMode
from .params import amp_cs_measurement_dc_params, amp_cs_measurement_ac_tran_params
from ac_tran_tb.testbench import testbench as ac_tran_testbench
from dc_tb.testbench import testbench as dc_testbench


class amp_cs_measurement_state(str, MeasurementState):
    PLOT = 'plot'
    END = 'end'

    @classmethod
    def initial_state(cls) -> MeasurementState:
        return cls.PLOT

    @classmethod
    def final_states(cls) -> Set[MeasurementState]:
        return {cls.END}


class amp_cs_measurement_dc(MeasurementBase):
    @classmethod
    def name(cls) -> str:
        return 'dc'

    @classmethod
    def description(cls) -> str:
        return 'Measurement class for DC plot.'

    @classmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        return amp_cs_measurement_dc_params

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return amp_cs_measurement_state

    @classmethod
    def dc_testbench_setup(cls) -> TestbenchSetup:
        return TestbenchSetup(
            testbench_name='dc_tb',
            testbench_class=dc_testbench,
            dut_wrapper=None
        )

    @classmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        return [
            cls.dc_testbench_setup()
        ]

    def __init__(self, params: amp_cs_measurement_dc_params):
        self.params = params

    def prepare_testbench(self,
                          current_state: amp_cs_measurement_state,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        setup = self.dc_testbench_setup()
        tb_params: dc_params = self.params.testbench_params_by_name['dc_tb'].copy()
        # no need to patch params
        return TestbenchRun(setup=setup, params=tb_params)

    def process_output(self,
                       current_state: amp_cs_measurement_state,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (amp_cs_measurement_state,
                                                        Dict[str, Any]):
        info("DC post-processing...")
        result_list = split_data_by_sweep(current_output, ['vin', 'vout'])

        plot_data_list = []
        for label, res_dict in result_list:
            cur_vin = res_dict['vin']
            cur_vout = res_dict['vout']

            cur_vin, vin_arg = np.unique(cur_vin, return_index=True)
            cur_vout = cur_vout[vin_arg]

            print(f"len(cur_vin)={len(cur_vin)}, len(cur_vout)={len(cur_vout)}")
            pprint(cur_vin)
            pprint(cur_vout)

            vout_fun = scipy.interpolate.InterpolatedUnivariateSpline(cur_vin, cur_vout)

            # MVM 20210305 in this case derivative(1) is the first derivative of the function
            # - which is what we need
            vout_diff_fun = vout_fun.derivative(1)

            print('gain_DC @vin=0.12V_manual=%.4g' % (abs(vout_diff_fun([0.12]))))  # MVM 20210305
            print('gain_DC_max for vin sim range=%.4g' % (max(abs(vout_diff_fun([cur_vin])[0]))))  # MVM 20210305

            plot_data_list.append((label, cur_vin, cur_vout, vout_diff_fun(cur_vin)))

        if self.params.plot:
            f, (ax1, ax2) = plt.subplots(2, sharex='all')
            ax1.set_title('Vout vs Vin')
            ax1.set_ylabel('Vout (V)')
            ax2.set_title('Gain vs Vin')
            ax2.set_ylabel('Gain (V/V)')
            ax2.set_xlabel('Vin (V)')

            for label, vin, vout, vout_diff_fun in plot_data_list:
                if label:
                    ax1.plot(vin, vout, label=label)
                    ax2.plot(vin, vout_diff_fun(vin), label=label)
                else:
                    ax1.plot(vin, vout)
                    ax2.plot(vin, vout_diff_fun(vin))

            if len(result_list) > 1:
                ax1.legend()
                ax2.legend()

            plt.show()

        return amp_cs_measurement_state.END, current_output


class amp_cs_measurement_ac_tran(MeasurementBase):
    @classmethod
    def name(cls) -> str:
        return 'ac_tran'

    @classmethod
    def description(cls) -> str:
        return 'Measurement class for AC plot.'

    @classmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        return amp_cs_measurement_ac_tran_params

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return amp_cs_measurement_state

    @classmethod
    def ac_tran_testbench_setup(cls) -> TestbenchSetup:
        return TestbenchSetup(
            testbench_name='ac_tran_tb',
            testbench_class=ac_tran_testbench,
            dut_wrapper=None
        )

    @classmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        return [
            cls.ac_tran_testbench_setup()
        ]

    def __init__(self, params: amp_cs_measurement_ac_tran_params):
        self.params = params

    def prepare_testbench(self,
                          current_state: amp_cs_measurement_state,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        setup = self.ac_tran_testbench_setup()
        tb_params: ac_tran_tb_params = self.params.testbench_params_by_name['ac_tran_tb'].copy()
        # no need to patch params
        return TestbenchRun(setup=setup, params=tb_params)

    def process_output(self,
                       current_state: amp_cs_measurement_state,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (amp_cs_measurement_state,
                                                        Dict[str, Any]):
        info("AC tran post-processing...")
        result_list = split_data_by_sweep(current_output, ['vout_tran'])

        tvec = current_output['time']
        plot_data_list = []
        for label, res_dict in result_list:
            cur_vout = res_dict['vout_tran']
            plot_data_list.append((label, cur_vout))

        if self.params.plot:
            plt.figure()
            plt.title('Vout vs Time')
            plt.ylabel('Vout (V)')
            plt.xlabel('Time (s)')

            for label, cur_vout in plot_data_list:
                if label:
                    plt.plot(tvec, cur_vout, label=label)
                else:
                    plt.plot(tvec, cur_vout)

            if len(result_list) > 1:
                plt.legend()

            plt.show()

        return amp_cs_measurement_state.END, current_output

