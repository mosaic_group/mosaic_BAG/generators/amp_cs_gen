from typing import *

from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackID
from .params import amp_cs_layout_params


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='amp_cs_layout_params parameter object',
        )

    def draw_layout(self):
        """Draw the layout."""

        params: amp_cs_layout_params = self.params['params']

        fg_amp = params.fg_dict['amp']
        fg_load = params.fg_dict['load']

        if fg_load % 2 != 0 or fg_amp % 2 != 0:
            raise ValueError('fg_load=%d and fg_amp=%d must all be even.' % (fg_load, fg_amp))

        # compute total number of fingers in each row
        fg_half_pmos = fg_load // 2
        fg_half_nmos = fg_amp // 2
        fg_half = max(fg_half_pmos, fg_half_nmos)
        fg_tot = (fg_half + params.ndum) * 2

        # specify width/threshold of each row
        nw_list = [params.w_dict['amp']]
        pw_list = [params.w_dict['load']]
        nth_list = [params.intent_dict['amp']]
        pth_list = [params.intent_dict['load']]

        # specify number of horizontal tracks for each row
        ng_tracks = [1]  # input track
        nds_tracks = [1]  # one track for space
        pds_tracks = [1]  # output track
        pg_tracks = [1]  # bias track

        # specify row orientations
        n_orient = ['R0']  # gate connection on bottom
        p_orient = ['MX']  # gate connection on top

        self.draw_base(params.lch, fg_tot, params.ptap_w, params.ntap_w, nw_list,
                       nth_list, pw_list, pth_list,
                       ng_tracks=ng_tracks, nds_tracks=nds_tracks,
                       pg_tracks=pg_tracks, pds_tracks=pds_tracks,
                       n_orientations=n_orient, p_orientations=p_orient,
                       )

        # figure out if output connects to drain or source of nmos
        if (fg_amp - fg_load) % 4 == 0:
            s_net, d_net = '', 'vout'
            aout, aoutb, nsdir, nddir = 'd', 's', 0, 2
        else:
            s_net, d_net = 'vout', ''
            aout, aoutb, nsdir, nddir = 's', 'd', 2, 0

        # create transistor connections
        load_col = params.ndum + fg_half - fg_half_pmos
        amp_col = params.ndum + fg_half - fg_half_nmos
        amp_ports = self.draw_mos_conn('nch', 0, amp_col, fg_amp, nsdir, nddir,
                                       s_net=s_net, d_net=d_net)
        load_ports = self.draw_mos_conn('pch', 0, load_col, fg_load, 2, 0,
                                        s_net='', d_net='vout')
        # amp_ports/load_ports are dictionaries of WireArrays representing
        # transistor ports.
        print(amp_ports)
        print(amp_ports['g'])

        # create TrackID from relative track index
        vin_tid = self.make_track_id('nch', 0, 'g', 0)
        vbias_tid = self.make_track_id('pch', 0, 'g', 0)
        # can also convert from relative to absolute track index
        print(self.get_track_index('nch', 0, 'g', 0))
        # get output track index, put it in the middle
        vout_bot = self.get_track_index('nch', 0, 'ds', 0)
        vout_top = self.get_track_index('pch', 0, 'ds', 0)
        vout_index = self.grid.get_middle_track(vout_bot, vout_top, round_up=True)
        vout_tid = TrackID(self.mos_conn_layer + 1, vout_index)

        vin_warr = self.connect_to_tracks(amp_ports['g'], vin_tid)
        vout_warr = self.connect_to_tracks([amp_ports[aout], load_ports['d']], vout_tid)
        vbias_warr = self.connect_to_tracks(load_ports['g'], vbias_tid)
        self.connect_to_substrate('ptap', amp_ports[aoutb])
        self.connect_to_substrate('ntap', load_ports['s'])

        vss_warrs, vdd_warrs = self.fill_dummy()

        self.add_pin('VSS', vss_warrs, show=params.show_pins)
        self.add_pin('VDD', vdd_warrs, show=params.show_pins)
        self.add_pin('vin', vin_warr, show=params.show_pins)
        self.add_pin('vout', vout_warr, show=params.show_pins)
        self.add_pin('vbias', vbias_warr, show=params.show_pins)

        # compute schematic parameters
        self._sch_params = dict(
            lch=params.lch,
            layout_params=params,
            dum_info=self.get_sch_dummy_info(),
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class amp_cs(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
