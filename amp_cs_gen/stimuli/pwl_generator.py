#! /usr/bin/env python3

import os
import sys

def gen_step_pwl(fname, td, tr, tpulse, amp):
    tvec = [0, td, td + tr, td + tr + tpulse, td + tr + tpulse + tr]
    yvec = [-amp, -amp, amp, amp, -amp]

    dir_name = os.path.dirname(fname)
    os.makedirs(dir_name, exist_ok=True)

    with open(fname, 'w') as f:
        for t, y in zip(tvec, yvec):
            f.write('%.8g %.8g\n' % (t, y))


def gen_pwl_data(outputFile):
    td = 100e-12
    tpulse = 800e-12
    tr = 20e-12
    amp = 10e-3

    tvec = [0, td, td + tr, td + tr + tpulse, td + tr + tpulse + tr]
    yvec = [-amp, -amp, amp, amp, -amp]

    for t, y in zip(tvec, yvec):
        outputFile.write('%.4g %.4g\n' % (t, y))

def main():
    fname = None  # TODO: allow argparse -o output.step
    if fname is None:
        gen_pwl_data(sys.stdout)
    else:
        dir_name = os.path.dirname(fname)
        os.makedirs(dir_name, exist_ok=True)

        with open(fname, 'w') as outputFile:
            gen_pwl_data(outputFile)

main()
